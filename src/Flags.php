<?php
declare(strict_types=1);

namespace Trick\ImageManager;

use \Nette\Utils\Image;

final class Flags
{
	public static function parse(string $flag): int
	{
		if ($flag == 'shrink_only') {
			return Image::ShrinkOnly;
		} else if ($flag == 'stretch') {
			return Image::Stretch;
		} else if ($flag == 'fit') {
			return Image::OrSmaller;
		} else if ($flag == 'fill') {
			return Image::OrBigger;
		} else if ($flag == 'exact') {
			return Image::Cover;
		} else {
			throw new \Exception('Unknow image flag "' . $flag . '".');
		}
	}
}
