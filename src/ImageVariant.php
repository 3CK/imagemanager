<?php
declare(strict_types=1);

namespace Trick\ImageManager;

class ImageVariant
{
	/** @var string */
	private $name;

	/** @var int */
	private $width = 500;

	/** @var int */
	private $height = 500;

	/** @var int Default 'fit'. */
	private $flag = 0b0000;

	/** @var string */
	private $saveDir;


	public function __construct(string $name, string $saveDir)
	{
		$this->name = $name;
		$this->saveDir = $saveDir . $name . '/';
	}

	public function setWidth(int $width): void
	{
		$this->width = $width;
	}

	public function getWidth(): int
	{
		return $this->width;
	}

	public function getHeight(): int
	{
		return $this->height;
	}

	public function setHeight(int $height): void
	{
		$this->height = $height;
	}

	public function getFlag(): int
	{
		return $this->flag;
	}

	public function setFlag(int $flag): void
	{
		$this->flag = $flag;
	}

	public function getDirectory(): string
	{
		return $this->saveDir;
	}

	public function getName(): string
	{
		return $this->name;
	}
}
