<?php
declare(strict_types=1);

namespace Trick\ImageManager;

class ImageSet
{
	/** @var string */
	private $fileName;

	/** @var ImageSettings */
	private $settings;

	/** @var array */
	private $images = [];


	public function __construct(string $fileName, array $settings)
	{
		$this->fileName = $fileName;
		$this->settings = Helpers::createSettingsFromArray($settings);

		foreach ($this->settings->getVariants() as $variant) {
			foreach ($this->settings->getGeneratedTypes() as $type) {
				$this->images[] = new Image($fileName, $variant, $type);
			}
		}
	}

	public function getSettings(): ImageSettings
	{
		return $this->settings;
	}

	public function getFileName(): string
	{
		return $this->fileName;
	}

	public function getSaveDirectory(): string
	{
		return $this->settings->getSaveDirectory();
	}

	public function getAllImages(): array
	{
		return $this->images;
	}

	public function getVariant(string $name): array
	{
		$return = [];

		foreach ($this->images as $image) {
			if ($image->getVariant()->getName() === $name) {
				$return[] = $image;
			}
		}

		if (empty($return)) {
			throw new \Exception('Image variant "' . $name . '" not found.');
		}

		return $return;
	}

	public function get(string $variant, string $extension): string
	{
		$images = $this->getVariant($variant);

		foreach ($images as $image) {
			if ($image->getType()->getExtension() === $extension) {
				return $image->getPath();
			}
		}

		throw new \Exception('Image not found (variant name "' . $variant . '", extension "' . $extension .'".');
	}
}
