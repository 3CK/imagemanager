<?php
declare(strict_types=1);

namespace Trick\ImageManager;

use Nette\Utils\Finder;
use Nette\Utils\Strings;
use Trick\ImageManager\ImageType\Gif;
use Trick\ImageManager\ImageType\Jpg;
use Trick\ImageManager\ImageType\Png;
use Trick\ImageManager\ImageType\Webp;

class Helpers
{

	public static function createSettingsFromArray(array $config): ImageSettings
	{
		$settings = new ImageSettings;

		if (array_key_exists('path', $config)) {
			$settings->setSaveDirectory($config['path']);
			unset($config['path']);
		} else if (defined('WWW_DIR')) {
			$settings->setSaveDirectory(WWW_DIR);
		} else {
			throw new \Exception('Where should i store images? Please define "path" value.');
		}

		if (array_key_exists('types', $config)) {
			foreach ($config['types'] as $type) {
				if ($type == 'jpg' || $type == 'jpeg') {
					$settings->addGeneratedType(new Jpg);
				} else if ($type == 'png') {
					$settings->addGeneratedType(new Png);
				} else if ($type == 'gif') {
					$settings->addGeneratedType(new Gif);
				} else if ($type == 'webp') {
					$settings->addGeneratedType(new Webp);
				} else {
					throw new \Exception('Unsupported image type "' . $type .'".');
				}
			}

			unset($config['types']);
		} else {
			$settings->addGeneratedType(new Webp);
			$settings->addGeneratedType(new Jpg);
		}

		if (array_key_exists('quality', $config)) {
			$settings->setQuality($config['quality']);
			unset($config['quality']);
		}

		foreach ($config as $name => $variantConfig) {
			$variant = new ImageVariant($name, $settings->getSaveDirectory());

			if (array_key_exists('width', $variantConfig)) {
				$variant->setWidth($variantConfig['width']);
			}

			if (array_key_exists('height', $variantConfig)) {
				$variant->setHeight($variantConfig['height']);
			}

			if (array_key_exists('flag', $variantConfig)) {
				$variant->setFlag(Flags::parse($variantConfig['flag']));
			}

			$settings->addVariant($variant);
		}

		return $settings;
	}

	/**
	 * Generates unique name from given text in $name or from original file name.
	 * Without an extension!
	 */
	public static function generateUniqueName(string $name, string $saveDirectory): string
	{
		$name = Strings::webalize($name);

		$i = 0;
		$originalName = $name;

		while (iterator_count(Finder::findFiles($name . '.*')->in($saveDirectory)) > 0) {
			$name = $originalName . '-' . $i;
			$i++;
		}

		return $name;
	}
}
