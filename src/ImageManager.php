<?php
declare(strict_types=1);

namespace Trick\ImageManager;

use Nette\Http\FileUpload;
use Nette\Utils\Finder;
use Nette\Utils\Image as NetteImage;
use Nette\Utils\ImageException;

class ImageManager
{
	/** @var string */
	private $wwwDir;

	public function __construct(string $wwwDir)
	{
		$this->wwwDir = $wwwDir;
	}

	public function upload(FileUpload $picture, array $config, string $name = null): string
	{
		if (!$picture->isOk() || $picture->getError() > 0) {
			throw new \Exception('An error has occured while uploading file to server [Error: ' . $picture->getError() . '].');
		} elseif (!$picture->isImage()) {
			throw new \Exception('Uploadovaný soubor není rastrový obrázek.');
		}

		$settings = Helpers::createSettingsFromArray($config);

		$givenName = $name ? $name : pathinfo($picture->getSanitizedName(), PATHINFO_FILENAME);
		$finalName = Helpers::generateUniqueName($givenName, $this->wwwDir . $settings->getSaveDirectory());

		$original = $this->wwwDir . $settings->getSaveDirectory() . $finalName . '.' . pathinfo($picture->getSanitizedName(), PATHINFO_EXTENSION);

		$picture->move($original);

		$image = NetteImage::fromFile($original);
		$successfulUploads = [];

		foreach ($settings->getVariants() as $variant) {
			$picture = clone $image; // We want to keep original file (not resized).
			$picture->resize($variant->getWidth(), $variant->getHeight(), $variant->getFlag());

			$dir = $this->wwwDir . $variant->getDirectory();
			if (!is_dir($dir)) {
				mkdir($dir);
			}

			$umask = umask(0);

			foreach ($settings->getGeneratedTypes() as $type) {
				$filePath = $this->wwwDir . $variant->getDirectory() . $finalName . '.' . $type->getExtension();

				try {
					$picture->save($filePath, $settings->getQuality(), $type->getInt());
					umask($umask);
					$successfulUploads[] = $filePath;
				} catch (ImageException $e) {
					$this->removeFiles($successfulUploads);
					throw new \Exception('Nepodařilo se vytvořit variantu "' . $variant->name .'", formát "' . $type->getExtension() . '"');
				}
			}
		}

		return $finalName;
	}

	public function remove(ImageSet $imageSet): void
	{
		foreach (Finder::findFiles($imageSet->getFileName() . '.*')->in($this->wwwDir . $imageSet->getSaveDirectory()) as $original => $foo) {
			unlink($original);
		}

		foreach ($imageSet->getAllImages() as $image) {
			unlink($this->wwwDir . $image->getPath());
		}
	}


	private function removeFiles(array $images): void
	{
		foreach ($images as $file) {
			unlink($file);
		}
	}


}
