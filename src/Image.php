<?php
declare(strict_types=1);

namespace Trick\ImageManager;

use Trick\ImageManager\ImageType\ImageType;

class Image
{
	/** @var string */
	private $fileName;

	/** @var ImageVariant */
	private $variant;

	/** @var ImageType */
	private $type;


	public function __construct(string $fileName, ImageVariant &$variant, ImageType &$type)
	{
		$this->fileName = $fileName;
		$this->variant = $variant;
		$this->type = $type;
	}

	public function getPath(): string
	{
		return $this->variant->getDirectory() . $this->fileName . '.' . $this->type->getExtension();
	}

	public function getType(): ImageType
	{
		return $this->type;
	}

	public function getVariant(): ImageVariant
	{
		return $this->variant;
	}
}
