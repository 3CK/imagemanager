<?php
declare(strict_types=1);

namespace Trick\ImageManager\ImageType;

final class Jpg extends ImageType
{
    public function getExtension(): string
    {
        return 'jpg';
    }

    public function getInt(): int
    {
        return IMAGETYPE_JPEG;
    }

    public function getMime(): string
    {
        return 'image/jpeg';
    }
}
