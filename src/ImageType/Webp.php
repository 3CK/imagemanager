<?php
declare(strict_types=1);

namespace Trick\ImageManager\ImageType;

final class Webp extends ImageType
{
    public function getExtension(): string
    {
        return 'webp';
    }

    public function getInt(): int
    {
        return IMAGETYPE_WEBP;
    }

    public function getMime(): string
    {
        return 'image/webp';
    }
}
