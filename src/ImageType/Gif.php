<?php
declare(strict_types=1);

namespace Trick\ImageManager\ImageType;

final class Gif extends ImageType
{
    public function getExtension(): string
    {
        return 'gif';
    }

    public function getInt(): int
    {
        return IMAGETYPE_GIF;
    }

    public function getMime(): string
    {
        return 'image/gif';
    }
}
