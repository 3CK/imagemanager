<?php
declare(strict_types=1);

namespace Trick\ImageManager\ImageType;

interface IImageType
{
    public function getExtension(): string;
    public function getInt(): int;
    public function getMime(): string;
}
