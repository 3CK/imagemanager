<?php
declare(strict_types=1);

namespace Trick\ImageManager\ImageType;

final class Png extends ImageType
{
    public function getExtension(): string
    {
        return 'png';
    }

    public function getInt(): int
    {
        return IMAGETYPE_PNG;
    }

    public function getMime(): string
    {
        return 'image/png';
    }
}
