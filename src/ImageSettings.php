<?php
declare(strict_types=1);

namespace Trick\ImageManager;

use Trick\ImageManager\ImageType\ImageType;

class ImageSettings
{
	/** @var string */
	private $saveDirectory;

	/** @var array */
	private $generatedTypes = [];

	/** @var array */
	private $variants = [];

	/** @var int */
	private $quality = 90;

	public function setSaveDirectory(string $path): void
	{
		$this->saveDirectory = $path;
	}

	public function getSaveDirectory(): string
	{
		return $this->saveDirectory;
	}

	public function addGeneratedType(ImageType $type): void
	{
		$this->generatedTypes[] = $type;
	}

	public function getGeneratedTypes(): array
	{
		return $this->generatedTypes;
	}

	public function addVariant(ImageVariant $variant): void
	{
		$this->variants[] = $variant;
	}

	public function getVariants(): array
	{
		return $this->variants;
	}

	public function setQuality(int $quality): void
	{
		$this->quality = $quality;
	}

	public function getQuality(): int
	{
		return $this->quality;
	}
}
